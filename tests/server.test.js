import IPCServer from '../src/server.js';
import { eventEnum } from '../src/enums.js';

const server = new IPCServer({
	log: {
		silent: false,
		debug: false,
	},
});

let dataMock = null;

server.on('listening', (socketPath) => {
	console.log(`Server listening on ${socketPath}`);
	server.createChannels(['test1', 'test2']);
});

server.on(eventEnum.CHANNEL_JOINED, ({ joinedChannels }) => {
	let i = 0;
	dataMock = setInterval(() => {
		server.send(joinedChannels[0], { i });
		i++;
	}, 1000);
});

server.on(eventEnum.CHANNEL_LEFT, () => {
	clearInterval(dataMock);
});
