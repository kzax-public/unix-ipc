import IPCClients from '../src/client.js';
import { eventEnum } from '../src/enums.js';

const client = new IPCClients({
	log: {
		silent: false,
		debug: true,
	},
});

client.on(eventEnum.SOCKET_CONNECTED, () => {
	client.join(['test1', 'test2']);
});
