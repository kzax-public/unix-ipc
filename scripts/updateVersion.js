import fs from 'fs';
import path from 'path';

try {
  const packageJsonPath = path.join(process.cwd(), 'package.json');
  const metaVersionPath = path.join(process.cwd(), 'src', 'metadata', 'version.js');
  const packageJsonData = fs.readFileSync(packageJsonPath, 'utf-8');
  const { version } = JSON.parse(packageJsonData);

  // Update version.js
  const versionContent = `export default Object.freeze({ lib: '${version}' });\n`;
  fs.writeFileSync(metaVersionPath, versionContent);

  console.log(`Updated version.js to version ${version}`);
} catch (error) {
  console.error(`Failed to update version: ${error}`);
}
