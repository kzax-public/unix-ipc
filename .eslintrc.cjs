module.exports = {
	extends: ['airbnb-base', 'eslint:recommended', 'prettier'],
	env: {
		node: true,
	},
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 13,
		requireConfigFile: false,
	},
	rules: {
		'import/extensions': ['error', 'always', { ignorePackages: true }],
		'no-console': 2,
		'import/no-extraneous-dependencies': 0,
		'import/prefer-default-export': 0,
		'no-bitwise': 0,
		'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 1,
		'no-unused-vars': process.env.NODE_ENV === 'production' ? 2 : 2,
		'no-unreachable': process.env.NODE_ENV === 'production' ? 2 : 2,
		'no-param-reassign': [0, { props: false }],
		'no-plusplus': 0,
		'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
		radix: 0,
	},
	ignorePatterns: ['node_modules/', 'lib/'],
};
