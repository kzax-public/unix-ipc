export const stringifyMessage = (message) => JSON.stringify(message);

export const parseMessage = (data) => JSON.parse(data);

export const prepareAndSendData = (socket, data) => {
	const preparedData = stringifyMessage(data);
	socket.write(preparedData);
};

export const prepareAndSendRawBuffer = (socket, data, encoding) => {
	const preparedData = Buffer.from(data, encoding);
	socket.write(preparedData);
};
