export const defaultOptions = {
	socketPath: '/tmp/unix-ipc.sock',
	heartbeatInterval: 5000,
	reconnect: 1000,
	rawBuffer: false,
	encoding: 'utf8',
	log: {
		silent: true,
		debug: false,
	},
};
