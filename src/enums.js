export const typeEnum = Object.freeze({
	JOIN_CHANNELS: 'join-channels',
	LEAVE_CHANNELS: 'leave-channels',
	CREATE_CHANNELS: 'create-channels',
	REMOVE_CHANNELS: 'remove-channels',
	DATA: 'data',
	ERROR: 'error',
});

export const eventEnum = Object.freeze({
	SERVER_SHUTDOWN: 'server-shutdown',
	CHANNEL_CREATED: 'channel-created',
	CHANNEL_REMOVED: 'channel-removed',
	CHANNEL_JOINED: 'channel-joined',
	CHANNEL_LEFT: 'channel-left',
	SOCKET_CONNECTED: 'socket-connected',
	SOCKET_DISCONNECTED: 'socket-disconnected',
	DATA: 'data',
	ERROR: 'error',
});
