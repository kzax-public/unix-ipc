import { eventEnum } from './enums.js';

const headerLength = 50;
const padStart = 0;

const formatHeader = (eventName) => {
	const eventNameWithSpaces = `${eventName.toUpperCase()} `;
	const eventNameSize = eventNameWithSpaces.length;

	const header = eventNameWithSpaces
		.padStart(eventNameSize + padStart, '-')
		.padEnd(headerLength, '-');
	return header;
};

export default (IPC) => {
	if (IPC.options.log.silent) return;

	IPC.on(eventEnum.SERVER_SHUTDOWN, () => {
		console.log(formatHeader(eventEnum.SERVER_SHUTDOWN));
	});

	IPC.on(eventEnum.CHANNEL_CREATED, (channel) => {
		console.log(formatHeader(eventEnum.CHANNEL_CREATED));
		console.log(' created channel:', channel);
	});

	IPC.on(eventEnum.CHANNEL_REMOVED, (channel) => {
		console.log(formatHeader(eventEnum.CHANNEL_REMOVED));
		console.log(' removed channel:', channel);
	});

	IPC.on(eventEnum.CHANNEL_JOINED, (data) => {
		console.log(formatHeader(eventEnum.CHANNEL_JOINED));
		console.log(' join channels:', data.joinedChannels);
		if (IPC.options.log.debug && data.socket) {
			console.log(data.socket);
		}
	});

	IPC.on(eventEnum.CHANNEL_LEFT, (data) => {
		console.log(formatHeader(eventEnum.CHANNEL_LEFT));
		console.log(' left channels:', data.leftChannels);
		if (IPC.options.log.debug && data.socket) {
			console.log(data.socket);
		}
	});

	IPC.on(eventEnum.SOCKET_CONNECTED, (data) => {
		console.log(formatHeader(eventEnum.SOCKET_CONNECTED));
		if (IPC.options.log.debug && data) {
			console.log(data);
		}
	});

	IPC.on(eventEnum.SOCKET_DISCONNECTED, (data) => {
		console.log(formatHeader(eventEnum.SOCKET_DISCONNECTED));
		if (IPC.options.log.debug && data) {
			console.log(data);
		}
	});

	IPC.on(eventEnum.DATA, (data) => {
		console.log(formatHeader(eventEnum.DATA));
		if (IPC.options.log.debug) {
			console.log(data);
		}
	});
	IPC.on('heartbeat', (data) => {
		console.log(formatHeader('heartbeat'));
		console.log(` sockets connected: ${data}`);
	});
};
