import net from 'node:net';
import { EventEmitter } from 'node:events';
import { defaultOptions } from './default.js';
import { typeEnum, eventEnum } from './enums.js';
import { parseMessage, prepareAndSendData } from './helpers.js';
import logger from './logger.js';

class IPCClient extends EventEmitter {
	#socket;
	#heartbeat;
	#logger;

	constructor(options = {}) {
		super();
		this.options = { ...defaultOptions, ...options };
		this.#logger = logger(this);
		this.channels = new Set();

		this.connect();
	}

	connect() {
		this.#socket = net.createConnection({ path: this.options.socketPath }, () => {
			this.emit(eventEnum.SOCKET_CONNECTED);
			this.#startHeartbeat();
		});

		this.#socket.on('data', (data) => {
			const { type, message } = parseMessage(data);
			this.emit(type, message);
		});

		this.#socket.on('end', () => {
			this.emit(eventEnum.SOCKET_DISCONNECTED);
			this.channels.clear();
		});
	}

	#startHeartbeat() {
		this.#heartbeat = setInterval(() => {
			if (!this.#socket.writable) {
				this.#socket.end();
				this.#stopHeartbeat();
			}
		}, this.options.heartbeatInterval);
	}

	#stopHeartbeat() {
		clearInterval(this.#heartbeat);
	}

	#sendToServer(type, message) {
		prepareAndSendData(this.#socket, { type, message });
	}

	send(message) {
		this.#sendToServer(typeEnum.MESSAGE, message);
	}

	join(channels) {
		this.#sendToServer(typeEnum.JOIN_CHANNELS, channels);
		channels.forEach((channel) => this.channels.add(channel));
	}

	leave(channels) {
		this.#sendToServer(typeEnum.LEAVE_CHANNELS, channels);
		channels.forEach((channel) => this.channels.delete(channel));
	}

	create(channels) {
		this.#sendToServer(typeEnum.CREATE_CHANNELS, channels);
	}
}

export default IPCClient;
