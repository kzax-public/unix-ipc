import fs from 'fs';
import process from 'process';

const cleanupAndExit = (socketPath) => {
	fs.unlink(socketPath, (err) => {
		if (err) {
			console.error(err);
		}
		process.exit();
	});
};

export default (socketPath) => {
	// Listen for the exit event
	process.on('exit', () => cleanupAndExit(socketPath));

	// Handle CTRL+C (SIGINT)
	process.on('SIGINT', () => cleanupAndExit(socketPath));

	// Handle terminate (SIGTERM)
	process.on('SIGTERM', () => cleanupAndExit(socketPath));

	// Handle uncaught exceptions
	process.on('uncaughtException', (err) => {
		console.error(`Uncaught Exception: ${err}`);
		cleanupAndExit(socketPath);
	});

	// Handle unhandled promise rejections
	process.on('unhandledRejection', (reason, promise) => {
		console.error(`Unhandled Rejection at: ${promise}, reason: ${reason}`);
		cleanupAndExit(socketPath);
	});
};
