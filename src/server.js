import net from 'node:net';
import { EventEmitter } from 'node:events';
import { defaultOptions } from './default.js';
import { typeEnum, eventEnum } from './enums.js';
import gracefulShutdown from './gracefulShutdown.js';
import { parseMessage, prepareAndSendData } from './helpers.js';
import logger from './logger.js';

export default class IPCServer extends EventEmitter {
	#heartbeat;
	#logger;

	constructor(options = {}) {
		super();
		this.options = { ...defaultOptions, ...options };
		this.#logger = logger(this);
		this.channels = new Map();
		this.socketsChannels = new Map();
		this.start();
	}

	start() {
		this.server = net.createServer((socket) => this.#addSocket(socket));
		gracefulShutdown(this.options.socketPath);

		this.server.listen(this.options.socketPath, () => {
			this.emit('listening', this.options.socketPath);
		});
	}

	shutdown() {
		this.socketsChannels.keys().forEach((socket) => {
			this.#sendToSocket(socket, eventEnum.SERVER_SHUTDOWN);
		});

		this.channels.clear();
		this.socketsChannels.clear();

		this.server.close(() => {
			this.emit(eventEnum.SERVER_SHUTDOWN);
		});
	}

	#startHeartbeat() {
		this.#heartbeat = setInterval(() => {
			[...this.socketsChannels.keys()].forEach((socket) => {
				if (!socket.writable) {
					this.#removeSocket(socket);
				}
			});

			if (this.socketsChannels.size === 0) {
				this.#stopHeartbeat();
			} else {
				this.emit('heartbeat', this.socketsChannels.size);
			}
		}, this.options.heartbeatInterval);
	}

	#stopHeartbeat() {
		clearInterval(this.#heartbeat);
	}

	#addSocket(socket) {
		if (this.socketsChannels.size === 0) {
			this.#startHeartbeat();
		}

		this.socketsChannels.set(socket, new Set());

		socket.on('data', (data) => {
			const { type, message } = parseMessage(data);
			switch (type) {
				case typeEnum.JOIN_CHANNELS: {
					const joinedChannels = this.#joinChannels(socket, message);
					this.#sendToSocket(socket, eventEnum.CHANNEL_JOINED, { joinedChannels });
					break;
				}
				case typeEnum.LEAVE_CHANNELS: {
					const leftChannels = this.#leaveChannels(socket, message);
					this.#sendToSocket(socket, eventEnum.CHANNEL_LEFT, { leftChannels });
					break;
				}
				case typeEnum.CREATE_CHANNELS: {
					const createdChannels = this.createChannels(message);
					this.#sendToSocket(socket, eventEnum.CHANNEL_CREATED, { createdChannels });
					break;
				}
				case typeEnum.REMOVE_CHANNELS: {
					const removedChannels = this.removeChannels(message);
					this.#sendToSocket(socket, eventEnum.CHANNEL_REMOVED, { removedChannels });
					break;
				}
				case typeEnum.DATA: {
					if (message.channel) {
						this.send(typeEnum.DATA, message, socket);
					} else {
						this.emit(eventEnum.DATA, message);
					}
					break;
				}
				default: {
					this.#sendToSocket(socket, typeEnum.ERROR, `Unknown message type: ${type}`);
					break;
				}
			}
		});

		socket.on('end', () => this.#removeSocket(socket));

		this.emit(eventEnum.SOCKET_CONNECTED, socket);
	}

	#removeSocket(socket) {
		const socketChannels = this.socketsChannels.get(socket);

		this.#leaveChannels(socket, socketChannels);
		this.socketsChannels.delete(socket);

		if (this.socketsChannels.size === 0) {
			this.#stopHeartbeat();
		}

		this.emit(eventEnum.SOCKET_DISCONNECTED, socket);
	}

	#sendToSocket(socket, type, message) {
		prepareAndSendData(socket, { type, message });
	}

	#joinChannels(socket, channels) {
		const joinedChannels = [];
		channels.forEach((channel) => {
			if (this.channels.has(channel)) {
				this.channels.get(channel).add(socket);
				this.socketsChannels.get(socket).add(channel);
				joinedChannels.push(channel);
			}
		});

		if (joinedChannels.length > 0) {
			this.emit(eventEnum.CHANNEL_JOINED, { joinedChannels, socket });
		}

		return joinedChannels;
	}

	#leaveChannels(socket, channels) {
		const leftChannels = [];
		channels.forEach((channel) => {
			if (this.channels.has(channel)) {
				this.channels.get(channel).delete(socket);
				this.socketsChannels.get(socket).delete(channel);
				leftChannels.push(channel);
			}
		});

		if (leftChannels.length > 0) {
			this.emit(eventEnum.CHANNEL_LEFT, { leftChannels, socket });
		}

		return leftChannels;
	}

	createChannels(channels) {
		const createdChannels = [];
		channels.forEach((channel) => {
			if (!this.channels.has(channel)) {
				this.channels.set(channel, new Set());
				createdChannels.push(channel);
			}
		});

		if (createdChannels.length > 0) {
			this.emit(eventEnum.CHANNEL_CREATED, createdChannels);
		}

		return createdChannels;
	}

	removeChannels(channels) {
		const removedChannels = [];
		channels.forEach((channel) => {
			if (this.channels.has(channel)) {
				const sockets = this.channels.get(channel);
				sockets.forEach((socket) => {
					this.#sendToSocket(socket, eventEnum.CHANNEL_REMOVED, channel);
				});
				this.channels.delete(channel);
				removeChannels.push(channel);
			}
		});

		if (removedChannels.length > 0) {
			this.emit(eventEnum.CHANNEL_REMOVED, removedChannels);
		}

		return removedChannels;
	}

	send(channel, data, senderSocket) {
		if (this.channels.has(channel)) {
			const sockets = this.channels.get(channel);

			sockets.forEach((socket) => {
				this.#sendToSocket(socket, typeEnum.DATA, { channel, data });
			});
		} else {
			const errorMessage = `Channel does not exist: ${channel}`;
			if (senderSocket) {
				this.#sendToSocket(senderSocket, typeEnum.ERROR, errorMessage);
			} else {
				throw new Error(errorMessage);
			}
		}
	}
}
